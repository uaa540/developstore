<%-- 
    Document   : registro_cliente_success
    Created on : 27/02/2016, 01:33:27 PM
    Author     : jesus
--%>

<%@page import="mx.com.develop.store.model.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: Inicio</title>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    </head>
    <body>
        <table width='800'>
            <tr bgcolor='#3882C7' align='center'>
                <td>
                    <h3><font color='white' >DevelopStore: Registro Correcto</h3> 
                </td>
            </tr>
            <tr align='right'>
                <td>
                    <b>Usted esta aqui: </b><a href='index.html'>Inicio</a>RegistroCorrecto
                </td>
            </tr>
        </table>
        <p>
            Te registraste de manera correcta con la siguiente información:
        </p>
        <%
        Cliente cliente = (Cliente)request.getAttribute("cliente");

        %>
        <b>Nombre: </b><%= cliente.getNombre() %><br/>
        <b>Edad: </b><%= cliente.getEdad() %><br/>
        <b>Direccion: </b><%= cliente.getDireccion() %><br/>
        <b>Telefono: </b><%= cliente.getTelefono() %><br/>
        <b>Usuario: </b><%= cliente.getUsuario() %><br/>
        <b>Contrasena: </b><%= cliente.getContrasenia() %><br/>  
    </body>
</html>

