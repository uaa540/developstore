<%-- 
    Document   : lista_productos
    Created on : 27/02/2016, 09:26:03 AM
    Author     : jesus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="mx.com.develop.store.model.Producto" %>
<%@page import="java.util.List" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: Inicio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <table width="800">
            <tr bgcolor="#3882C7" align="center">
                <td>
                    <h3><font color="white" >DevelopStore: Listado de productos</h3> 
                </td>
            </tr>
            <tr align="right">
                <td>
                    <table>
                        <tr>
                            <td>Usuario: ${cliente.nombre} </td>
                            <td>
                               
                            </td>
                            <%--<td><%= request.getParameter("usuario") %></td>--%>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
        <b>Usted está aquí:</b><a href="index.html">Inicio</a> /Listado de Productos
        <h2>Lista de productos</h2>
        <table border="1" id="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Descripción</th>
                    <th>Tipo</th>
                    <th>Color</th>
                    <th>Talla</th>
                    <th>Precio</th>
                    <th>Disponibles</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <%
                 List<Producto> produtos = (List<Producto>)getServletContext().getAttribute("productos");
                 int i = 0;
                 for(Producto producto:produtos){
                    i++; 
                %>
                <tr>
                    <td><%= i%></td>
                    <td><%= producto.getDescripcion()%></td>
                    <td><%= producto.getTipo().getTitulo()%></td>
                    <td><%= producto.getColor().getTitulo()%></td>
                    <td><%= producto.getTalla()%></td>
                    <td>$<%= producto.getPrecio()%></td>
                    <td><%= producto.getDisponibles() %></td>
                    <td><a href="ventas/detalles_producto.view?id=<%= producto.getId() %>">
                            <img src="imagenes/carrito.png" width="40" height="40" alt="
                             carrito">
                    </td>
                </tr>
                <%}%>
            </tbody>
        </table>
    </body>
</html>