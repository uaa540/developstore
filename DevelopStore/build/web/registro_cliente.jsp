<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: Inicio</title>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    </head>
    <body>
        <table width='800'>
            <tr bgcolor='#3882C7' align='center'>
                <td>
                    <h3><font color='white' >DevelopStore: Registro de Clientes</h3> 
                </td>
            </tr>
            <tr align='right'>
                <td>
                    <b>Usted esta aqui: </b><a href='index.html'>Inicio</a>Registro
                </td>
            </tr>
        </table>

        <form action='registro.do' method='POST'>
            <table>
                <tr>
                    <td>Nombre:</td>
                    <td><input type='text' name='nombre' value='' size='20' /></td>
                </tr>
                <tr>
                    <td>Edad:</td>
                    <td><input type='text' name='edad' value='' size='20' /></td>
                </tr>
                <tr>
                    <td>Dirección:</td>
                    <td><input type='text' name='direccion' value='' size='40' /></td>
                </tr>
                <tr>
                    <td>Teléfono:</td>
                    <td><input type='text' name='telefono' value='' size='15' /></td>
                </tr>
                <tr>
                    <td>Usuario:</td>
                    <td><input type='text' name='usuario' value='' size='20' /></td>
                </tr>
                <tr>
                    <td>Contraseña:</td>
                    <td><input type='password' name='contrasenia' value='' size='20' /></td>
                </tr>

                <tr>
                    <td>
                        <img src="captcha.png" width="137" height="56"/>
                    </td>
                    <td>
                        Escribe el texto:</br>
                        <input type="text" value="" name="captcha" size="20"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type='submit' name='submit' value='Registrar' /></td>
                </tr>
            </table>

        </form>
    </body>
</html>

