<%-- 
    Document   : lista_productos_error
    Created on : 12/03/2016, 01:20:25 PM
    Author     : jesus
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: Error</title>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    </head>
    <body>
        <table width='800'>
            <tr bgcolor='#FF0000' align='center'>
                <td>
                    <h3><font color='white' >DevelopStore: ERROR PRODUCTO</h3> 
                </td>
            </tr>
            <tr align='right'>
                <td>
                    <b>Usted esta aqui: </b><a href='../index.html'>Inicio</a>Registro
                </td>
            </tr>
        </table>

        <h3>Corregir errores</h3>
        <%
            List<String> listaErrores = (List<String>) request.getAttribute("listErrores");

        %>
        <font color="red">
        <ul>
            <%                
                for (String error : listaErrores) {
            %>
            <li>
                <%= error%>
            </li>
         <%}%>
        </ul>
        </font>

    </body>
</html>


