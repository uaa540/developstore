<%-- 
    Document   : detalles_producto
    Created on : 19/03/2016, 01:09:30 PM
    Author     : jesus
--%>

<%@page import="mx.com.develop.store.model.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: Inicio</title>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    </head>
    <body>
        <table width='800'>
            <tr bgcolor='#3882C7' align='center'>
                <td>
                    <h3><font color='white' >DevelopStore: Detalles Productos</h3> 
                </td>
            </tr>
            <tr align='right'>
                <td>
                    <b>Usted esta aqui: </b><a href='../index.html'>Inicio</a>DetallesProducto
                </td>
            </tr>
        </table>
        <p>
            Producto registrado satisfactoriamente:
        </p>
        <%
        Producto producto = (Producto)request.getAttribute("producto");

        %>
        <b>Color: </b><%= producto.getColor().getTitulo() %><br/>
        <b>Precio: </b><%= producto.getPrecio()  %><br/>
        <b>Talla: </b><%= producto.getTalla() %><br/>
        <b>Descripcion: </b><%= producto.getDescripcion() %><br/>
        <b>Tipo: </b><%= producto.getTipo().getTitulo() %><br/>
        
        <form action="agregar_carrito.do?id=${producto.id}" method="POST">
            <tbody> 
                <tr>
                    <td>Cantidad: </td>
                    <td>
                        <select>
                          <% 
                           int disponibles = producto.getDisponibles();
                           for(int i = 1; i<=disponibles;i++){
                          %> 
                          <option><%= i %></option>
                          <%}%>
                        </select>
                    </td>  
                </tr>
                <tr>
                    <td><input type="submit" value="Agrerar a carrito"></td>>
                </tr>
            </tbody>

        </form>
    </body>
</html>


