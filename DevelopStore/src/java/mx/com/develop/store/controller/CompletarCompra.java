package mx.com.develop.store.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.develop.store.model.Cliente;
import mx.com.develop.store.model.Factura;
import mx.com.develop.store.model.Producto;
import mx.com.develop.store.model.Venta;

@WebServlet(urlPatterns = {"/ventas/completar_compra.do"})
public class CompletarCompra extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //addCode
        //obtener session
        HttpSession session = request.getSession(false);
        ServletContext context = getServletContext();
        if (session != null) {
            //addCode
            Venta venta = (Venta)session.getAttribute("venta");
            if (venta != null) {
                Map<Producto, Integer> productos = venta.getProductos();
                double total = 0.0;
                Set<Producto> set = productos.keySet();
                for (Producto producto : set) {
                    total += producto.getPrecio();
                }
                //cupones
                String cupones = "fjie46,jac68A,kjdfg89";
                //vaciar el carrito
                session.removeAttribute("venta");
                request.setAttribute("venta", venta);
                request.setAttribute("total", total);
                request.setAttribute("cupones", cupones);
                
                Factura factura = new Factura();
                Cliente cliente = (Cliente)session.getAttribute("cliente");
                factura.setCliente(cliente);
                request.setAttribute("factura", factura);
                request.getRequestDispatcher("completar_compra.jsp").forward(request, response);
            } else {
                venta = new Venta();
                response.sendRedirect("lista_carrito_error.jsp");
            }
        } else {
            response.sendRedirect("lista_carrito_error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
