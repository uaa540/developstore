
package mx.com.develop.store.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import javax.servlet.RequestDispatcher;
import mx.com.develop.store.model.Color;
import mx.com.develop.store.model.Producto;
import mx.com.develop.store.model.Talla;
import mx.com.develop.store.model.TipoProducto;
import org.jboss.weld.logging.Category;


@WebServlet(name = "ListaProductos", urlPatterns = {"/lista_productos.view"})
public class ListaProductos extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        List<Producto> productos = new LinkedList<Producto>();

        String usuario = "Tovar Lerma J Jesus";
        
        request.setAttribute("usuario", usuario);
        //Pasar la lista
        //request.setAttribute("productos", productos);
        
        String ContextPath = request.getContextPath();
        System.out.println("ContextPath: "+ContextPath);
        System.out.println("RequestURI: "+request.getRequestURI());
        System.out.println("RequestURL: "+request.getRequestURL());
        System.out.println("ServletPath: "+request.getServletPath());
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("lista_productos.jsp");
        dispatcher.forward(request, response);
        response.sendRedirect("http://www.google.com.mx");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
