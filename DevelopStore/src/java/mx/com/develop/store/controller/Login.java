package mx.com.develop.store.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.develop.store.model.Cliente;

@WebServlet(name = "Login", urlPatterns = {"/login.do"})
public class Login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String usuarioParametro = request.getParameter("usuario");
        String contraseniaParametro = request.getParameter("contrasenia");

        ServletContext context = getServletContext();
        List<Cliente> clientes = (List<Cliente>) context.getAttribute("clientes");

        if (clientes == null) {
            response.sendRedirect("login_error.jsp");
        } else {
            boolean encontrado = false;
            for (Cliente cliente : clientes) {
                if (usuarioParametro.equals(cliente.getUsuario())
                        && contraseniaParametro.equals(cliente.getContrasenia())) {
                    HttpSession session = request.getSession();
                    //session.setMaxInactiveInterval(1*60);
                    session.setAttribute("cliente", cliente);
                    Cookie cookie = new Cookie("JSESSIONID", session.getId());
                    cookie.setMaxAge(15*60);
                    response.addCookie(cookie);
                    encontrado = true;
                }
            }

            if (encontrado) {
                response.sendRedirect("lista_productos.view");
            } else {
                response.sendRedirect("login_error.jsp");
            }
        }

    }
 
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
