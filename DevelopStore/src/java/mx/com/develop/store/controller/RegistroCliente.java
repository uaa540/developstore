package mx.com.develop.store.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import javax.security.auth.message.callback.PrivateKeyCallback;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.develop.store.model.Cliente;

//@WebServlet(name = "RegistroCliente", urlPatterns = {"/registro.do"})
public class RegistroCliente extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String captcha = request.getParameter("captcha");
        //obtener contexto
        ServletContext context = request.getServletContext();
        
        List<Cliente> clientes = (List<Cliente>)context.getAttribute("clientes");

        if (clientes==null) {
            clientes = new LinkedList<>();
        }
        Enumeration<String> captchas = context.getInitParameterNames();

        boolean captchaCorrecta = false;
        while (captchas.hasMoreElements()) {
            String c = captchas.nextElement();
            if (context.getInitParameter(c).equals(captcha)) {
                captchaCorrecta = true;
                break;
            }
        }

        if (captchaCorrecta) {

            String nombre = request.getParameter("nombre");
            System.out.println("nombre: " + nombre);
            int edad = Integer.parseInt(request.getParameter("edad"));
            String direccion = request.getParameter("direccion");
            String telefono = request.getParameter("telefono");
            String usuario = request.getParameter("usuario");
            String contrasena = request.getParameter("contrasenia");

            Cliente cliente = new Cliente(nombre, edad, direccion, telefono,
                    usuario, contrasena);
            clientes.add(cliente);
            request.setAttribute("cliente", cliente);
            context.setAttribute("clientes", clientes);
            
            request.getRequestDispatcher("registro_cliente_success.jsp").forward(request, response);
        } else {
            response.sendRedirect("catpcha_errors.jsp");
        }

    }

    public RegistroCliente() {
        System.out.println("HOla");
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("En public void init(ServletConfig config)");
        super.init(config);

    }

    @Override
    public void init() throws ServletException {
        System.out.println("En public void init()");
        super.init();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        System.out.println("En service(ServletRequest req, ServletResponse res)");
        super.service(req, res);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("En protected void service(HttpServletRequest req, HttpServletResponse resp)");
        //super.service(req, resp); 
        doPost(req, resp);
    }

    @Override
    public void destroy() {
        System.out.println("En public void destroy()");
        super.destroy(); //To change body of generated methods, choose Tools | Templates.
    }

}
