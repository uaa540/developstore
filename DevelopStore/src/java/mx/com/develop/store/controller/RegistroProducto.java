package mx.com.develop.store.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.com.develop.store.model.Color;
import mx.com.develop.store.model.Producto;
import mx.com.develop.store.model.Talla;
import mx.com.develop.store.model.TipoProducto;

@WebServlet(name = "RegistroProducto", urlPatterns = {"/admin/registro_productos.do"})
public class RegistroProducto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<String> listErrores = new ArrayList<String>();
        
        ServletContext contex = getServletContext();
        List<Producto> productos = (List<Producto>) contex.getAttribute("productos");
        
        if (productos == null) {
            productos = new LinkedList<>();
            //contex.setAttribute("productos", productos);
        } 
         
//variables
        Color color = null;
        double precio = 0.0;
        Talla talla = null;
        String descripcion = null;
        TipoProducto tipo = null;
        //selecciona
        try {
            color = Color.valueOf(request.getParameter("color"));
        } catch (IllegalArgumentException iae) {
            listErrores.add("Debes seleccionar un color, es OBLIGATORIO");
        }

        try {
            precio = new Double(request.getParameter("precio"));
        } catch (NumberFormatException nfe) {
            listErrores.add("El numero debe ser un double");
        }

        try {
            talla = Talla.valueOf(request.getParameter("talla"));
        } catch (Exception e) {
            listErrores.add("Selecciona una talla.");
        }
        
        descripcion = request.getParameter("descripcion");

        try {
            tipo = TipoProducto.valueOf(request.getParameter("tipo"));
        } catch (IllegalArgumentException iae) {
            listErrores.add("Seleccionar un tipo.");
        }

        if (listErrores.isEmpty()) {
            Producto producto = new Producto(color, precio, talla, descripcion, tipo);
            request.setAttribute("producto", producto);
            productos.add(producto);
            contex.setAttribute("productos", productos);
            
            request.getRequestDispatcher("lista_producto_success.jsp").forward(request, response);
        } else {
            request.setAttribute("listErrores", listErrores);
            request.getRequestDispatcher("lista_productos_error.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
