package mx.com.develop.store.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.com.develop.store.model.Producto;
import mx.com.develop.store.model.Venta;

/**
 *
 * @author jesus
 */
@WebServlet(name = "AgregarCarrito", urlPatterns = {"/ventas/agregar_carrito.do"})
public class AgregarCarrito extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        System.out.println("Tiempo de Creación "
                + new Date(session.getCreationTime()));
        System.out.println("Ultima petición: "
                + new Date(session.getLastAccessedTime()));
        if (session != null) {
            if (session.getAttribute("cliente") != null) {
                ServletContext context = getServletContext();
                List<Producto> productos = (List<Producto>) context.getAttribute("productos");
                Integer idProducto = Integer.parseInt(request.getParameter("id"));
                for (Producto producto : productos) {
                    if (producto.getId().equals(idProducto)) {
                        Integer cantidad = Integer.parseInt(request.getParameter("cantidad"));
                        producto.setDisponibles(producto.getDisponibles() - cantidad);

                        Venta venta = (Venta) session.getAttribute("venta");
                        if (venta == null) {
                            venta = new Venta();
                            session.setAttribute("venta", venta);
                        }
                        Map<Producto, Integer> ventaProductos = venta.getProductos();
                        if (ventaProductos.get(producto) != null) {
                            ventaProductos.put(producto, ventaProductos.get(producto) + cantidad);
                        } else {
                            ventaProductos.put(producto, cantidad);
                        }
                        request.getRequestDispatcher("lista_carrito.jsp").forward(request, response);

                    }
                }
            } else {
                response.sendRedirect("agregar_carrito_error.jsp");

            }

        } else {
            response.sendRedirect("detalles_producto_error.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
