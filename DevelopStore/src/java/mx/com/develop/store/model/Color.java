package mx.com.develop.store.model;

public enum Color {
    ROJO("Rojo"), AZUL("Azul"), VERDE("Verde"), MORADO("Cafe"), NARANJA("Naranja"),
    CAFE("Cafe"), NEGRO("Negro"), BLANCO("Blanco"), AMARILLO("Amarillo"), 
    VIOLETA("Violeta");

    private String titulo;

    private Color(String titulo) {
        this.titulo = titulo;
    }
    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}
