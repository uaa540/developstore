
package mx.com.develop.store.model;

public enum TipoProducto {
 BLUSA("Blusa"),PLAYERA("Playera"),PANTALON("Pantalon"),CAMISA("Camisa");
 
    private String titulo;

    private TipoProducto(String titulo) {
        this.titulo = titulo;
    }
    
    

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

}
