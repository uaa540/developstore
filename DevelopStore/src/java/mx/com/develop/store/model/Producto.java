package mx.com.develop.store.model;

public class Producto {

    private Integer id;

    private Color Color;

    private double precio;

    private Talla talla;

    private String descripcion;

    private TipoProducto tipo;

    private int disponibles;

    public int getDisponibles() {
        return disponibles;
    }

    public void setDisponibles(int disponibles) {
        this.disponibles = disponibles;
    }

    public Producto() {
    }

    public Producto(Integer id, Color Color, double precio, Talla talla, String descripcion, TipoProducto tipo, int disponibles) {
        this.id = id;
        this.Color = Color;
        this.precio = precio;
        this.talla = talla;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.disponibles = disponibles;
    }

    public Producto(Color Color, double precio, Talla talla, String descripcion, TipoProducto tipo) {
        this.Color = Color;
        this.precio = precio;
        this.talla = talla;
        this.descripcion = descripcion;
        this.tipo = tipo;
    }

    public TipoProducto getTipo() {
        return tipo;
    }

    public void setTipo(TipoProducto tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Talla getTalla() {
        return talla;
    }

    public void setTalla(Talla talla) {
        this.talla = talla;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Color getColor() {
        return Color;
    }

    public void setColor(Color Color) {
        this.Color = Color;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
