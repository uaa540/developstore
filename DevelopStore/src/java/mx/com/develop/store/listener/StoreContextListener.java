
package mx.com.develop.store.listener;

import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import mx.com.develop.store.model.Color;
import mx.com.develop.store.model.Producto;
import mx.com.develop.store.model.Talla;
import mx.com.develop.store.model.TipoProducto;


@WebListener()
public class StoreContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        ServletRegistration.Dynamic dynamicServlet = context.addServlet("RegistroCliente", 
                "mx.com.develop.store.controller.RegistroCliente");
        dynamicServlet.addMapping("/registro.do");
        
        List<Producto> productos = new LinkedList<Producto>();
        productos.add(new Producto(1, Color.ROJO, 167.50, Talla.CHICA, "Playera con estampado de Disney.", TipoProducto.PLAYERA,5));
        productos.add(new Producto(2, Color.AZUL, 199.99, Talla.GRANDE, "PantalÃ³n de mezclilla.", TipoProducto.PANTALON,4));
        productos.add(new Producto(3, Color.MORADO, 257.99, Talla.MEDIANA, "Blusa morada con cuello V.", TipoProducto.BLUSA,8));
        productos.add(new Producto(4, Color.CAFE, 174.50, Talla.GRANDE, "Camisa con boton premium.", TipoProducto.CAMISA,4));
        productos.add(new Producto(5, Color.NEGRO, 480.99, Talla.XG, "Playera para caballero con estampado de Dinosaurio.", TipoProducto.PLAYERA,2));

        context.setAttribute("productos", productos);
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
