<%-- 
    Document   : lista_productos
    Created on : 27/02/2016, 09:26:03 AM
    Author     : jesus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="mx.com.develop.store.model.Producto" %>
<%@page import="java.util.List" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: Lista Productos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <table width="800">
            <tr bgcolor="#3882C7" align="center">
                <td>
                    <h3><font color="white" >DevelopStore: Listado de productos</h3> 
                </td>
            </tr>
            <tr align="right">
                <td>
                    <table>
                        <tr>
                            <td>
                                <c:if test="${sessionScope.cliente ne null}">
                                    <a href="ventas/lista_carrito.jsp" >Ver carrito de compras</a>  
                                </c:if>

                            </td>
                            <td>Usuario: <c:out value="${sessionScope.cliente.nombre}" escapeXml="false">
                                    <b>Invitado</b></c:out>

                                <td>

                                </td>
                            <%--<td><%= request.getParameter("usuario") %></td>--%>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <b>Usted está aquí:</b><a href="index.html">Inicio</a> /Listado de Productos
        <h2>Lista de productos</h2>
        <table border="1" id="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Descripción</th>
                    <th>Tipo</th>
                    <th>Color</th>
                    <th>Talla</th>
                    <th>Precio</th>
                    <th>Disponibles</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                <c:forEach items="${productos}" var="producto" varStatus="index">
                    <tr id="td">
                        <td>${index.count}</td>
                        <td>${producto.descripcion}</td>
                        <td>${producto.tipo.titulo}</td>
                        <td>${producto.color.titulo}</td>
                        <td>${producto.talla}</td>
                        <td>${producto.precio}</td>
                        <td>${producto.disponibles}</td>
                        <td><a href="ventas/detalles_producto.view?id=${producto.id}">
                                <img src="imagenes/carrito.png" width="40" height="40" alt="
                                     carrito">
                                </td>
                                </tr>
                            </c:forEach>


                            </tbody>
                            </table>
                            </body>
                            </html>