<%-- 
    Document   : detalles_producto
    Created on : 19/03/2016, 01:09:30 PM
    Author     : jesus
--%>

<%@page import="mx.com.develop.store.model.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: DetallesProducto</title>
        <meta charset='UTF-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    </head>
    <body>
        <table width='800'>
            <tr bgcolor='#3882C7' align='center'>
                <td>
                    <h3><font color='white' >DevelopStore: Detalles Productos</h3> 
                </td>
            </tr>
            <tr align='right'>
                <td>
                    <table>
                        <tr>
                            <td></td>
                            <td>
                                Usuario: ${cliente.nombre} <a href="../logout.do">Salir</a>
                            </td>
                        </tr>
                    </table>
                </td> 
            </tr>
        </table>

        <b>Usted está aquí:</b> <a href="../index.html">Inicio</a>/
        <a href="../lista_productos.view">Listado de Productos</a>/Detalles del Producto</a><br/>

    <p>
        Producto registrado satisfactoriamente:
    </p>
    <%
        Producto producto = (Producto) request.getAttribute("producto");

    %>
    
    <b>ID: </b>${param.id}<br/>
    <b>Color: </b>${producto["color"].titulo}<br/>
    <b>Precio: </b>${producto.precio}<br/>
    <b>Talla:</b> ${ producto["talla"]}</br>
    <b>Descripcion:</b> ${producto["descripcion"]}</br>
    <b>Tipo:</b> ${producto["tipo"].titulo}</br>  

    <form action="agregar_carrito.do?id=${producto.id}" method="POST">
        <table border="0">
            <tbody> 
                <tr>
                    <td>Cantidad: </td>
                    <td>
                        <select name="cantidad">
                            <%
                                int disponibles = producto.getDisponibles();
                                for (int i = 1; i <= disponibles; i++) {
                            %> 
                            <option><%= i%></option>
                            <%}%>
                        </select>
                    </td>  
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Agrerar a carrito"></td>
                </tr>
            </tbody>
        </table>

    </form>
</body>
</html>


