
<%@page import="mx.com.develop.store.model.TipoProducto"%>
<%@page import="mx.com.develop.store.model.Talla"%>
<%@page import="mx.com.develop.store.model.Color"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Develop Store: Inicio</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <table width="800">
            <tr bgcolor="#3882C7" align="center">
                <td>
                    <h3><font color="white" >DevelopStore: Registro de productos</h3> 
                </td>
            </tr>
            <tr align="right">
                <td>
                    <table>
                        <tr>
                            <td><a href="../login.html">Login</a></td>
                            <td><a href="../registro_cliente.html">Registrarse</a></td>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>

        <b>Usted está aquí:</b><a href="../index.html">Inicio</a>/
        Admin/Registro de Productos
        
        <h2>Los campos con * son obligatorios</h2>
        <form action="registro_productos.do" method="POST" >
            <table>
                <tr>
                    <td>* Color:</td>
                    <td>
                        <select name="color">
                            <option>Selecciona...</option>
                            <%
                                Color colores[] = Color.values();
                                for (Color color : colores) {

                            %>
                            <option value="<%= color%>"><%= color.getTitulo() %></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>* Precio:</td>
                    <td>
                        <input type="text" name="precio"/>
                    </td>
                </tr>
                <tr>
                    <td>* Talla:</td>
                    <td>
                        <select name="talla">
                            <option>Selecciona...</option>
                            <%
                            Talla tallas[] = Talla.values();
                            for(Talla talla:tallas){
                            %>
                            <option value="<%= talla%>"><%= talla %></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Descripción:</td>
                    <td>
                        <textarea name="descripcion" rows="6" cols="30"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>* Tipo:</td>
                    <td>
                        <select name="tipo">
                            <option>Selecciona...</option>
                            <%
                            TipoProducto tipos[] = TipoProducto.values();
                            for(TipoProducto tipo:tipos){
                            %>
                            <option value="<%= tipo%>"><%= tipo.getTitulo() %></option>
                            <%}%>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <input type="submit" value="Registrar" />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
